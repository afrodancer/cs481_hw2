import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Kamen Rider Gaim', style: TextStyle(fontWeight: FontWeight.bold,),
                  ),
                ),
              Text('The path of blossoms! To the stage!', style: TextStyle(
                color: Colors.grey[500],),
              ),
              ],
            ),
          ),
          Icon(Icons.thumb_up, color: Colors.blue[500],),
          Text('23'),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;

    Row _buildButtonColumn(Color color, IconData icon, String label) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.all(8),
            child: Text(
              label,
                style:TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: color,
                ),
            ),
          ),
        ],
      );
    }

    Widget buttonSection = Container(
      margin: const EdgeInsets.only(left: 32),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildButtonColumn(color, Icons.chat_bubble, 'This is my stage now!'),
          _buildButtonColumn(color, Icons.chat_bubble, 'This power exists to protect those who cannot fight!'),
          _buildButtonColumn(color, Icons.chat_bubble, 'It does not matter who is on my side! I am still protecting \nthe same people! Even if I completely change in the end!'),
        ],
      ),
    );

    Widget textSection1 = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'While the superheroes of Marvel and DC have claimed international recognition,'
            'there are others that have gained their own fame within Japan and its '
            'neighboring countries. The TV series known as Kamen Rider (Kah-Men Ri-Dur) '
            'is produced by Toei (Toh-Ey), which is actually the same company that '
            'gave us the original source material for the Power Rangers TV show, '
            'which is called Super Sentai (sen-tie). \n\nMuch like Super Sentai, '
            'Kamen Rider revolves around a protagonist who is granted a belt that '
            'allows them to transform into the superhero known as Kamen Rider. It '
            'is a live action show with special effects that airs every sunday for '
            'an entire year. In this particular season, Kamen Rider Gaim (Guy-muh) '
            'revolves around the theme of fruit, and the orange happens to be the '
            'main fruit that the hero uses the most. \n\n',
        softWrap: true,
      ),
    );

    Widget textSection2 = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
            'So what makes this show '
            'any different from Power Rangers or Super Sentai? For starters, the '
            'suits have more "armor" on them compared to the simplified spandex '
            'suits of the other heroes. Each season of Kamen Rider also follows '
            'its own story arc, and the episodes follow the story sequentially. '
            'For Super Sentai, while there usually is a general overarching story, '
            'each episode involves them facing a challenge brought about by the '
            'monster in that episode, and then fighting said monster, so you could '
            'watch most of them out of order. '
            'Kamen Rider is generally aimed at older boys in Japan, so the stories '
            'sometimes tackle some more mature themes, while Super Sentai is aimed '
            'at a slightly younger audience. \n\n',
        softWrap: true,
      ),
    );

    Widget textSection3 = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
            'Like Super Sentai though, the '
            'show is produced with the idea of selling toys alongside the release '
            'of the show. Super Sentai and Kamen Rider both sell a variety of '
            'merchandise, including action figures of the heroes, the robots they '
            'pilot in the shows, and even the transformation '
            'devices that they use. And when a new character is introduced, or the '
            'main character acquires a '
            'new power-up in the show, it usually means a new line of toys '
            'for them to sell on the same day the new episode comes out. Rather than '
            'just release all the toys at the start of the season, the company works'
            'closely with toy sellers to release relevant merchandise only after'
            'certain episodes air to help build up hype.',
        softWrap: true,
      ),
    );

    Widget textSection4 = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'But the toys are not the only appeal of these shows. While the goal of the '
        'show is to help sell the toys, the writers also do their best to create a '
        'story with an interesting theme to it. In the case of Kamen Rider Gaim, one '
        'of the themes the show brings up is the idea of helping others even if they have '
        'done wrong to you. The main protagonist usually ends up going through several '
        'trials that challenge their idea of what they should be fighting for. In general '
        'the writers try to make the heroes someone that the audience can look up to, '
        'whether it is kids or adults, to help them become a more kind adult.',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Layout Homework 2',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework 2'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'assets/images/kamenRiderGaim.png',
              width: 600,
              height: 350,
              fit: BoxFit.cover,
            ),
           titleSection,
           buttonSection,
           textSection1,
            Image.asset('assets/images/sentaiKamen.jpg',
              width: 600,
              height: 250,
              fit: BoxFit.fitWidth,
            ),
            textSection2,
            Image.asset('assets/images/driverGaim.jpg',
              width: 600,
              height: 250,
              fit: BoxFit.fitWidth,
            ),
            textSection3,
            Image.asset('assets/images/kouta.png',
              width: 600,
              height: 250,
              fit: BoxFit.fitWidth,
            ),
            textSection4,
          ]
        ),
      ),
    );
  }
} //MyApp